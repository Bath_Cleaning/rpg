// プログラムは WinMain から始まります
#include "Game.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	ChangeWindowMode(TRUE);
	SetGraphMode(1280, 960, 16);

	/*CMenu* pSysMenu = GetSystemMenu(FALSE);
	pSysMenu->EnableMenuItem(SC_CLOSE, MF_GRAYED);*/

	if (DxLib_Init() == -1)		// ＤＸライブラリ初期化処理
	{
		return -1;			// エラーが起きたら直ちに終了
	}
	SetDrawScreen(DX_SCREEN_BACK);
	//ChangeFontType(DX_FONTTYPE_ANTIALIASING_EDGE)

	XINPUT_STATE input;



	Player *p;
	p = new Player();

	Map *map;
	map = new Map();


	bool endflag = false;

	while (endflag == false) {
		ClearDrawScreen();

		GetJoypadXInputState(DX_INPUT_PAD1, &input);
		p->P_Move(input);


		map->M_Draw();

		p->P_Draw();

		if (ProcessMessage() == -1)
		{
			break;        // エラーが起きたらループを抜ける
		}

		if (CheckHitKey(KEY_INPUT_ESCAPE))
		{
			endflag = true;
		}

		ScreenFlip();
	}


	delete p;
	delete map;

	DxLib_End();				// ＤＸライブラリ使用の終了処理

	return 0;				// ソフトの終了 
}