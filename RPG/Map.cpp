#include "Map.h"

Map::Map()
{
	count = 0;

	roomHmax = (MAP_HEIGHT / ROOM_H_MAX) - 3;
	roomHmin = (MAP_HEIGHT / ROOM_H_MAX) / 3;
	roomWmax = (MAP_WIDTH / ROOM_W_MAX) - 3;
	roomWmin = (MAP_WIDTH / ROOM_W_MAX) / 3;

	//データ初期化
	for (int i = 0; i < MAP_HEIGHT; i++) {
		for (int j = 0; j < MAP_WIDTH; j++) {
			mapdata[i][j] = 0;
		}
	}
	//マップ生成
	MapCreate();
}

Map::~Map()
{

}
//マップ生成
void Map::MapCreate()
{
	//部屋通過初期化
	for (int i = 0; i < ROOM_H_MAX+2; i++) {
		for (int j = 0; j < ROOM_W_MAX+2; j++) {
			roadflag[i][j] = false;
		}
	}

	//部屋生成
	for (int i = 0; i < ROOM_H_MAX;i++) {
		for (int j = 0; j < ROOM_W_MAX;j++) {
			//部屋の最低幅、高さ以上最高以下の大きさ
			roomH[i][j] = roomHmin + GetRand(roomHmax - roomHmin);
			roomW[i][j] = roomWmin + GetRand(roomWmax - roomWmin);

			//場所 (位置*全マップ幅、高さ/部屋数+2)を最低値、部屋最大区画から-2した位置を最高区画に
			roomX[i][j] = (j*MAP_WIDTH / ROOM_W_MAX + 2)+ GetRand((j*MAP_WIDTH / ROOM_W_MAX + MAP_WIDTH / ROOM_W_MAX - roomW[i][j] - 2)- (j*MAP_WIDTH / ROOM_W_MAX + 2));
			roomY[i][j] = (i*MAP_HEIGHT / ROOM_H_MAX + 2) + GetRand((i*MAP_HEIGHT / ROOM_H_MAX + MAP_HEIGHT / ROOM_H_MAX - roomH[i][j] - 2)- (i*MAP_HEIGHT / ROOM_H_MAX + 2));

			RoomCreate(j,i, roomX[i][j], roomY[i][j]);
		}
	}

	//部屋通過判定
	RoomPass(GetRand(ROOM_W_MAX-1), GetRand(ROOM_H_MAX-1));

	//道生成
	for (int i = 0; i < ROOM_H_MAX; i++) {
		for (int j = 0; j < ROOM_W_MAX; j++) {
			RoadCreate(j, i);
		}
	}
}

//部屋作成
void Map::RoomCreate(int w, int h,int x,int y)
{
	for (int i = y; i < roomH[h][w] + y;i++) {
		for (int j = x; j < roomW[h][w] + x;j++) {
			mapdata[i][j]= 1;
		}
	}
}

//部屋通過
void Map::RoomPass(int w, int h)
{
	roompasscount = 0;
	roompassflag[h][w] = true;
	for (int i = 0; i < ROOM_H_MAX; i++) {
		for (int j = 0; j < ROOM_W_MAX; j++) {
			if (roompassflag[i][j] == true) {
				roompasscount += 1;
			}
		}
	}

	if (roompasscount == 9) {
		return;
	}

	bool loop = true;
	int loopcount = 0;

	while (loop == true) {
		switch (GetRand(3))
		{
		case UP:
			if (h != 0) {
				roadflag[h + (h - 1)][w * 2] = true;
				RoomPass(w, h - 1);
				loop = false;
			}
			break;
		case DOWN:
			if (h != ROOM_H_MAX - 1) {
				roadflag[h * 2 + 1][w * 2] = true;
				RoomPass(w, h + 1);
				loop = false;
			}
			break;
		case LEFT:
			if (w != 0) {
				roadflag[h * 2][w + (w - 1)] = true;
				RoomPass(w - 1, h);
				loop = false;
			}

			break;
		case RIGHT:
			if (w != ROOM_W_MAX - 1) {
				roadflag[h * 2][w * 2 + 1] = true;
				RoomPass(w + 1, h);
				loop = false;
			}
			break;
		}
		loopcount++;
	}

}

//道生成
void Map::RoadCreate(int w,int h)
{
	//部屋の間(上下)に通路あるかどうか
	if (roadflag[h + (h - 1)][w * 2] == 1) {
		//上向き道
		roadX[UP][h][w] = roomX[h][w] + GetRand(roomW[h][w] - 1);
		roadY[UP][h][w] = roomY[h][w] - 1;

		for (int i = 0; i < roadY[UP][h][w] - (MAP_HEIGHT / ROOM_H_MAX * h); i++) {
			mapdata[roadY[UP][h][w] - i][roadX[UP][h][w]] = 2;
		}

		//下向き道
		roadX[DOWN][h-1][w] = roomX[h-1][w] + GetRand(roomW[h-1][w] - 1);
		roadY[DOWN][h-1][w] = roomY[h-1][w] + roomH[h-1][w];

		for (int i = 0; i < (MAP_HEIGHT / ROOM_H_MAX * h) - roadY[DOWN][h - 1][w]; i++) {
			mapdata[roadY[DOWN][h - 1][w] + i][roadX[DOWN][h - 1][w]] = 2;
		}

		//連結
		if (roadX[DOWN][h - 1][w] > roadX[UP][h][w]) {
			for (int i = 0; i < roadX[DOWN][h - 1][w] - roadX[UP][h][w] + 1; i++) {
				mapdata[(MAP_HEIGHT / ROOM_H_MAX * h)][roadX[UP][h][w] + i] = 2;
			}
		}
		else if (roadX[DOWN][h - 1][w] <= roadX[UP][h][w]) {
			for (int i = 0; i < roadX[UP][h][w] - roadX[DOWN][h - 1][w] + 1; i++) {
				mapdata[(MAP_HEIGHT / ROOM_H_MAX * h)][roadX[DOWN][h - 1][w] + i] = 2;
			}
		}
	}

	//部屋の間(左右)に通路あるかどうか
	if ((roadflag[h * 2][w + (w - 1)] == 1) &&(w != 0)){

		//左向き道
		roadX[LEFT][h][w] = roomX[h][w] - 1;
		roadY[LEFT][h][w] = roomY[h][w] + GetRand(roomH[h][w] - 1);

		for (int i = 0; i < roadX[LEFT][h][w] - (MAP_WIDTH / ROOM_W_MAX * w); i++) {
			mapdata[roadY[LEFT][h][w]][roadX[LEFT][h][w] - i] = 2;
		}

		//右向き道
		roadX[RIGHT][h][w - 1] = roomX[h][w-1] + roomW[h][w-1];
		roadY[RIGHT][h][w - 1] = roomY[h][w-1] + GetRand(roomH[h][w-1] - 1);

		for (int i = 0; i < (MAP_WIDTH / ROOM_W_MAX * w) - roadX[RIGHT][h][w - 1]; i++) {
			mapdata[roadY[RIGHT][h][w - 1]][roadX[RIGHT][h][w - 1] + i] = 2;
		}

		//連結
		if (roadY[RIGHT][h][w - 1] > roadY[LEFT][h][w]) {
			for (int i = 0; i < roadY[RIGHT][h][w - 1] - roadY[LEFT][h][w] + 1; i++) {
				mapdata[roadY[LEFT][h][w] + i][(MAP_WIDTH / ROOM_W_MAX * w)] = 2;
			}
		}
		else if (roadY[RIGHT][h][w - 1] <= roadY[LEFT][h][w]) {
			for (int i = 0; i < roadY[LEFT][h][w] - roadY[RIGHT][h][w - 1] + 1; i++) {
				mapdata[roadY[RIGHT][h][w - 1] + i][(MAP_WIDTH / ROOM_W_MAX * w)] = 2;
			}
		}
	}

	/*
	if (h != 0) {
		roadX[UP][h][w] = roomX[h][w] + GetRand(roomW[h][w] - 1);
		roadY[UP][h][w] = roomY[h][w] - 1;
		

		for (int j = 0; j < roadY[UP][h][w] - (MAP_HEIGHT / ROOM_H_MAX * h); j++) {
			mapdata[roadY[UP][h][w] - j][roadX[UP][h][w]] = 2;
		}
	}

	if (h != ROOM_H_MAX - 1) {
		roadX[DOWN][h][w] = roomX[h][w] + GetRand(roomW[h][w] - 1);
		roadY[DOWN][h][w] = roomY[h][w] + roomH[h][w];


		for (int j = 0; j < (MAP_HEIGHT / ROOM_H_MAX * (h + 1)) - roadY[DOWN][h][w]; j++) {
			mapdata[roadY[DOWN][h][w] + j][roadX[DOWN][h][w]] = 2;
		}
	}

	if (w != 0) {
		roadX[LEFT][h][w] = roomX[h][w] - 1;
		roadY[LEFT][h][w] = roomY[h][w] + GetRand(roomH[h][w] - 1);

		for (int j = 0; j < roadX[LEFT][h][w] - (MAP_WIDTH / ROOM_W_MAX * w); j++) {
			mapdata[roadY[LEFT][h][w]][roadX[LEFT][h][w] - j] = 2;
		}
	}

	if (w != ROOM_W_MAX - 1) {
		roadX[RIGHT][h][w] = roomX[h][w] + roomW[h][w];
		roadY[RIGHT][h][w] = roomY[h][w] + GetRand(roomH[h][w] - 1);


		for (int j = 0; j < (MAP_WIDTH / ROOM_W_MAX * (w + 1)) - roadX[RIGHT][h][w]; j++) {
			mapdata[roadY[RIGHT][h][w]][roadX[RIGHT][h][w] + j] = 2;
		}
	}
	*/


}

//マップ描画
void Map::M_Draw()
{
	for (int i = 0; i < MAP_HEIGHT; i++) {
		for (int j = 0; j < MAP_WIDTH; j++) {
			switch (mapdata[i][j])
			{
			case 0:
				DrawBox(j*MAP_SIZE, i*MAP_SIZE, j* MAP_SIZE+MAP_SIZE, i* MAP_SIZE + MAP_SIZE, GetColor(0, 0, 255), true);
				break;
			case 1:
				DrawBox(j*MAP_SIZE, i*MAP_SIZE, j* MAP_SIZE + MAP_SIZE, i* MAP_SIZE + MAP_SIZE, GetColor(0, 255, 0), true);
				break;
			case 2:
				DrawBox(j*MAP_SIZE, i*MAP_SIZE, j* MAP_SIZE + MAP_SIZE, i* MAP_SIZE + MAP_SIZE, GetColor(255, 0, 0), true);
				break;
			case 3:
				DrawBox(j*MAP_SIZE, i*MAP_SIZE, j* MAP_SIZE + MAP_SIZE, i* MAP_SIZE + MAP_SIZE, GetColor(255, 255, 0), true);
				break;
			}
		}
	}

	DrawFormatString(0, 100, GetColor(255, 255, 255), "passcount:%d", roompasscount);
	/*
	DrawFormatString(0, 100, GetColor(255, 255, 255), "roomH[1][0]:%d", roomH[0][0]);
	DrawFormatString(150, 100, GetColor(255, 255, 255), "roomW[1][0]:%d", roomW[0][0]);
	*/

	//部屋通過確認
	for (int i = 0; i < ROOM_H_MAX + 2; i++) {
		for (int j = 0; j < ROOM_W_MAX + 2; j++) {
			DrawFormatString(200+(j*30), 200+(i*30), GetColor(255, 255, 255), "%d", roadflag[i][j]);
		}
	}
}

int Map::GetMapData(int w, int h)
{
	return mapdata[h][w];
}
