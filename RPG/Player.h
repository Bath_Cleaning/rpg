#pragma once
#include"Character.h"

class Player :public Character
{
private:
	vector<vector<int>>playerdata; //プレイヤーデータ入れ
	string str;

	int x, y;//座標
	
	int height, width;
	
	int count;
	int speed;
	bool moveflag;
	direction P_direction;

	void P_DataIn();

public:
	Player();
	~Player();

	void P_Move(XINPUT_STATE Input);//プレイヤー移動
	void P_Draw();//描画

	int GetPlayerX();
	int GetPlayerY();

	void SetPlayerX(int x);
	void SetPlayerY(int y);
};


