#pragma once
#include "conf.h"
//125,100
const int MAP_WIDTH = 40 * 2;
const int MAP_HEIGHT = 30 * 2;
const int ROOM_H_MAX = 3;
const int ROOM_W_MAX = 3;


class Map {
private:
	int mapdata[MAP_HEIGHT][MAP_WIDTH];
	
	//部屋位置
	int roomX[ROOM_H_MAX][ROOM_W_MAX];
	int roomY[ROOM_H_MAX][ROOM_W_MAX];
	int roomH[ROOM_H_MAX][ROOM_W_MAX];//縦
	int roomW[ROOM_H_MAX][ROOM_W_MAX];//横

	int roomHmax;//部屋の縦最大
	int roomHmin;//部屋縦最小
	int roomWmax;//部屋横最大
	int roomWmin;//横最小

	int count;

	//道
	int roadX[4][ROOM_H_MAX][ROOM_W_MAX];
	int roadY[4][ROOM_H_MAX][ROOM_W_MAX];


	bool roompassflag[ROOM_H_MAX][ROOM_W_MAX]; //部屋通過フラグ
	int roompasscount;//通過部屋数
	bool roadflag[ROOM_H_MAX + ROOM_H_MAX-1][ROOM_W_MAX+ROOM_W_MAX-1]; //通路存在

	//vector<vector<int>>mapdata;//マップデータ入れ
	//string str;
	//int height, width;

public:
	Map();
	~Map();

	void MapCreate();//マップ生成
	void RoomCreate(int w,int h,int x,int y);//部屋作成,部屋の幅、高さ、場所を引数
	void RoomPass(int w, int h);//部屋通過
	void RoadCreate(int w, int h);//道生成

	void M_Draw();//描画

	int GetMapData(int w,int h);
};