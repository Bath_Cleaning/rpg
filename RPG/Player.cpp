#include "Player.h"

Player::Player()
{
	moveflag = false;
	speed = 32;
	count = 0;
	P_direction = UP;

	ifstream ifs("PlayerData.csv");

	if (!ifs) {
		return;
	}


	for (int i = 0; getline(ifs, str); i++)
	{
		height = i + 1;
		playerdata.resize(height);
		string tmp;
		istringstream stream(str);

		for (int j = 0; getline(stream, tmp,','); j++)
		{

			width = j + 1;
			playerdata[i].resize(width);
			//string型のマップデータをchar型に変えてint型に変える
			playerdata[i][j] = stoi(tmp);
		}

	}
	P_DataIn();
}

Player::~Player()
{

}

void Player::P_DataIn()
{
	hp = playerdata[0][1];
	mp = playerdata[0][2];
	x = playerdata[0][3];
	y = playerdata[0][4];
}

//移動処理
void Player::P_Move(XINPUT_STATE Input)
{
	//動いてなければ移動をする
	if (moveflag == false) {
		if ((Input.Buttons[XINPUT_BUTTON_DPAD_UP] == 1) || (CheckHitKey(KEY_INPUT_UP) == 1)) {
			P_direction = UP;
			moveflag = true;
		}
		else if ((Input.Buttons[XINPUT_BUTTON_DPAD_DOWN] == 1) || (CheckHitKey(KEY_INPUT_DOWN) == 1)) {
			P_direction = DOWN;
			moveflag = true;
		}
		else if ((Input.Buttons[XINPUT_BUTTON_DPAD_LEFT] == 1) || (CheckHitKey(KEY_INPUT_LEFT) == 1)) {
			P_direction = LEFT;
			moveflag = true;
		}
		else if ((Input.Buttons[XINPUT_BUTTON_DPAD_RIGHT] == 1) || (CheckHitKey(KEY_INPUT_RIGHT) == 1)) {
			P_direction = RIGHT;
			moveflag = true;
		}
	}
	//8カウントで動かす
	else if(count < 8){
		switch (P_direction)
		{
		case UP:
			y -= speed/8;
			break;
		case DOWN:
			y += speed/8;
			break;
		case LEFT:
			x -= speed/8;
			break;
		case RIGHT:
			x += speed/8;
			break;
		}
		count++;
	}
	//移動状態とカウントを戻す
	else {
		count = 0;
		moveflag = false;
	}
}

void Player::P_Draw()
{
	DrawBox(x,y,x+ MAP_SIZE,y+ MAP_SIZE,GetColor(255,255,255),true);

	/*
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width;j++) {
			DrawFormatString(j*150, i*20, GetColor(255, 255, 255), "playerdata:%d", playerdata[i][j]);
		}
	}
	*/


	DrawFormatString(0, 60, GetColor(255, 255, 255), "x:%d", x);
	DrawFormatString(0, 90, GetColor(255, 255, 255), "y:%d", y);


	/*
	DrawFormatString(0, 0, GetColor(255, 255, 255), "playerdata:%d", playerdata.size());
	DrawFormatString(0,30, GetColor(255, 255, 255), "playerdata[i]:%d", playerdata[0].size());
	DrawFormatString(0, 60, GetColor(255, 255, 255), "height:%d", height);
	DrawFormatString(0, 90, GetColor(255, 255, 255), "width:%d", width);
	*/
}

int Player::GetPlayerX()
{
	return x;
}

int Player::GetPlayerY()
{
	return y;
}

void Player::SetPlayerX(int x)
{
	this->x = x;
}

void Player::SetPlayerY(int y)
{
	this->y = y;
}
